# Landing Maker
Creamos este proyecto como muestra para el trabajo en equipo con Git :)

## Preséntate maker
Estimado maker, puedes presentarte en la página de nosotros. Para ello, debes
llenar el siguiente fragmento de código y colocarlo en `nosotros.html`.

__Obs:__ para la fotografía, puedes colocarla dentro de la carpeta `img`.

```html
      <div class="card member">
        <img src="img/foto.png" alt="" srcset="" class="image-c" />
        <div class="text-member">
          <h3>Mi nombre</h3>
          <p>
            ¿Quién soy?
            ¿Qué temas de tecnología me gustan?
            Mis pasatiempos
            Sobre qué me gusta enseñar
          </p>
        </div>
      </div>
```

La sección en donde debes colocarlo es:
```html
    <section id="nosotros" class="card-members">
        ...
        </codigo>
        ...
    </section>
```
